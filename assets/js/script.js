/* Author:

*/
jQuery(document).ready(function(){
	var winWidth = jQuery(window).width();

	jQuery("#parentlink").on("click", function(){
			jQuery(".dDown").fadeToggle( "slow", "linear" );
	});


	//TOGGLE FORM
	jQuery("#toggle-adv").on("click", function(){
		jQuery("#colapse").slideToggle("slow", function(){
			if(jQuery(this).css('display') == "none"){
				jQuery("#toggle-adv span").text("click here to use our advanced search")
			} else{
				jQuery("#toggle-adv span").text("advanced search")
			}
		});
	});

	//PLACEHOLDER
	jQuery(function(){
		jQuery.placeholder.shim();
	});

	//MAIN SLIDER
	jQuery('.bxslider').bxSlider({
		mode: 'fade'
	});

	//MAGZ SLIDER
	jQuery(".mgslide-item").bxSlider({
	   minSlides: 2,
  		maxSlides: 2,
	  slideWidth: 800,
	  slideMargin: 10
	});

	//CAROUSEL
	jQuery('.bxcarousel').bxSlider({
		 mode: 'horizontal',
		  minSlides: 1,
		  maxSlides: 3,
		  slideWidth: 300,
		  slideMargin: 10
	});

	//CHECKBOX
	jQuery('input[type=radio], input[type=checkbox]').customRadioCheck();

	var thischeck = jQuery('ul.list-interest li .check');
	jQuery(thischeck).on("click", function(){
		jQuery(this).parent().parent().toggleClass("active");	
	});
	 

	var dragWidthMinFull = false;
	var dragWidthMaxFull = false;
	
	//SCROLL PANE HORIZONTAL WHEEL
	var elmentPane = jQuery('.scroll-pane').jScrollPane({
		horizontalDragMinWidth: 250,
		horizontalDragMaxWidth: 250,
		showArrows: true
	});


		 	/*var apis = [];
			jQuery(window).on(
				'resize',
				function()
				{
					if (winWidth < 480){
						if (apis.length) {
							jQuery.each(
								apis,
								function(i) {
									jQuery('.scroll-pane').destroy();
								}
							)
							apis = [];
						}
						return false;
					}
				}
			);*/


	 jQuery('.scroll-panegrid').jScrollPane({
			showArrows: false
	 });

	 if(jQuery(".figthumb").length){
	 		jQuery(".mainBookItem").removeClass("full");
	 }else{
	 		jQuery(".mainBookItem").addClass("full");
	 		jQuery(".bookitem").css('display', 'none');
	 }
	 /*var api = element.data('jsp');
	 element.bind(
	      'mousewheel',
	      function (event, delta, deltaX, deltaY)
	      {
	          api.scrollByX(delta *-50);
	          return false;
	      }
	 );*/
	jQuery('#pane').jScrollPane(
		{	autoReinitialise: true, 
			showArrows: true, 
	});
	
	//SELECTBOX
	jQuery(".selectId").selectbox();

	//IMG LIQUID
	jQuery(".imgFill").imgLiquid({fill:true});
	jQuery(".imgNotFill").imgLiquid({fill:false});
	var clientH = jQuery(".nw-client").height();
	if ( !(winWidth < 767)){
			jQuery(window).on(" load resize", function(){
				jQuery(".f-signin").css({
					'height' : clientH
				});
			});
	}

	//EQUAL HEIGHT
	if ( !(winWidth < 767)){
		taller = 0;
		var equal = jQuery(".equalH");
		equal.each(function(){
			thisHeight = jQuery(this).innerHeight();
			if (thisHeight > taller) {
				 taller = thisHeight
			};
		});
		equal.innerHeight(taller);
	}
	
	//ZOOM
	jQuery(".zoom").elevateZoom({
  		 zoomType : "lens", 
  		 lensShape : "round", 
  		 lensSize : 250,
  		 gallery:'galleryzoom', 
  		 cursor: 'pointer', 
  		 galleryActiveClass: 'active', 
  		 imageCrossfade: true, 
	});

	jQuery(".zoom").bind("click", function(e) { 
		var ez = jQuery('.zoom').data('elevateZoom');	
		jQuery.fancybox(ez.getGalleryList()); 
		return false; 
	}); 

	// initialize
	var $container = jQuery('#freewall');
	$container.masonry({
	  itemSelector: '.brick',
	  "gutter": 5
	});

	/*jQuery('#book_glery').masonry({
	  itemSelector: '.bookitem',
	  "gutter": 5
	});

	jQuery('#book_glery').isotope({
	    layoutMode: 'masonryHorizontal',
	    itemSelector: '.bookitem',
	    masonryHorizontal: {
	      rowHeight: 100
	    }
  	});*/

	/*initialize
	jQuery('#book_glery').masonry({
	  itemSelector: '.bookitem',
	  //columnWidth: '.ab_item',
	  //columnWidth: ".grid-sizer",
	  gutter: 5
	  //"isFitWidth": true
	  //"isOriginLeft": false,
	});*/




	jQuery("<p class='titleslide-xs visible-xs'>Browse Areas</p>").insertBefore("#intro .bx-controls");

	//TOGGLE TABLE

	var parent = jQuery(".tb-toggle").parent();
	jQuery(".nav-toggle").on("click", function(){
		jQuery(".tb-toggle").toggleClass("active");
			if(jQuery(".tb-toggle").css('display')=='none'){
				jQuery(".nav-toggle").html('VIEW DETAILS');
			} else{
				jQuery(".nav-toggle").html('HIDE DETAILS');
			}
	});

	jQuery(window).load(function(){
		jQuery('.popCart').appendTo('#container');
		
		jQuery('<div/>',{
			id: 'shadcart'
		}).appendTo('#container');

		jQuery('<span/>',{
			"class" : "popClose",
			 click:function(){
				jQuery('.popCart').fadeOut();  
				jQuery('#shadcart').fadeOut();  
			}
		}).appendTo('.popCart');
	});

	jQuery(".cartToggle").on("click", function(e){
		//e.stopPropagation();
		jQuery(".popCart").fadeToggle("fast", "linear");
		jQuery("#shadcart").fadeToggle();
	});
	jQuery('.closePop').on('click',function(e){
		/*if(e.target.className !== "#cartToggle" ) {
		      jQuery('.popCart', '#shadcart').fadeOut();  
		}*/
		jQuery('.popCart').fadeOut();  
	});

	jQuery(window).bind("scroll", function(){
		var sticky_offset = jQuery('#toprow').height() + 180;
		// our function that decides weather the navigation bar should have "fixed" css position or not.
		var scroll_top = jQuery(window).scrollTop(); // our current vertical position from the top
		// if we've scrolled more than the navigation, change its position to fixed to stick to top, otherwise change it back to relative
		if (scroll_top > sticky_offset) { 
			jQuery('#fixed-toprow').addClass("motion").css("top", 0);
		}else{
			jQuery('#fixed-toprow').css("top", "-500px");
		} 	
	});

	jQuery(".toglefaq-st").on("click", function(){
		jQuery(this).toggleClass("open");
		jQuery(this).parent().find(".desc").slideToggle("slow");
	});

	jQuery("#menu > li").each(function(i){
			jQuery(this).attr('id', 'id_'+(1+i));
	});

	/*jQuery('.columnize').column({
		width:150,
		rule_style: 'solid'
	});*/

});




